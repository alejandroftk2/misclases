
package misclases;

public class Contrato {
    private int numeroContrato;
    private int tipoContrato;
    private int nivelEstudios;
    private int diasTrabajados;
    private String fechaContrato;
    private String nombreEmpleado;
    private String domicilio;
    private float pagoDiarioBase;
    
    Contrato() {
        this.numeroContrato = 0;
        this.tipoContrato = 0;
        this.nivelEstudios = 0;
        this.diasTrabajados = 0;
        this.fechaContrato = "No especificado";
        this.nombreEmpleado = "No especificado";
        this.domicilio = "No especificado";
        this.pagoDiarioBase = 0.0f;
    }

    Contrato(int numeroContrato, int tipoContrato, int nivelEstudios, int diasTrabajados, String fechaContrato, String nombreEmpleado, String domicilio, float pagoDiarioBase) {
        this.numeroContrato = numeroContrato;
        this.tipoContrato = tipoContrato;
        this.nivelEstudios = nivelEstudios;
        this.diasTrabajados = diasTrabajados;
        this.fechaContrato = fechaContrato;
        this.nombreEmpleado = nombreEmpleado;
        this.domicilio = domicilio;
        this.pagoDiarioBase = pagoDiarioBase;
    }
    
    Contrato(Contrato x){
        this.numeroContrato = x.numeroContrato;
        this.tipoContrato = x.tipoContrato;
        this.nivelEstudios = x.nivelEstudios;
        this.diasTrabajados = x.diasTrabajados;
        this.fechaContrato = x.fechaContrato;
        this.nombreEmpleado = x.nombreEmpleado;
        this.domicilio = x.domicilio;
        this.pagoDiarioBase = x.pagoDiarioBase;
    }

    public int getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(int numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public int getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(int tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public int getNivelEstudios() {
        return nivelEstudios;
    }

    public void setNivelEstudios(int nivelEstudios) {
        this.nivelEstudios = nivelEstudios;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }

    public String getFechaContrato() {
        return fechaContrato;
    }

    public void setFechaContrato(String fechaContrato) {
        this.fechaContrato = fechaContrato;
    }

    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public float getPagoDiarioBase() {
        return pagoDiarioBase;
    }

    public void setPagoDiarioBase(float pagoDiarioBase) {
        this.pagoDiarioBase = pagoDiarioBase;
    }
   
    public float calcularSubtotal(){
        float subtotal = 0.0f;
        if(this.nivelEstudios == 1){
            subtotal = this.pagoDiarioBase * 1.2f * this.diasTrabajados;
        } else if(this.nivelEstudios == 2) {
            subtotal = this.pagoDiarioBase * 1.5f * this.diasTrabajados;
        } else {
            subtotal = this.pagoDiarioBase * 2 * this.diasTrabajados;
        }
        return subtotal;
    }
    
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = this.calcularSubtotal() * 0.16f;
        return impuesto;
    }
    
    public float calcularTotalAPagar(){
        float totalAPagar = 0.0f; 
        totalAPagar = this.calcularSubtotal() - this.calcularImpuesto();
        return totalAPagar;
    }
    
    public void imprimirContrato(){
        System.out.println("Numero de contrato: " + numeroContrato);
        System.out.println("Nombre del empleado: " + nombreEmpleado);
        System.out.println("Domicilio: " + domicilio);
        System.out.println("Tipo de contrato: " + tipoContrato);
        System.out.println("Nivel de estudios: " + nivelEstudios);
        System.out.println("Dias trabajados: " + diasTrabajados);
        System.out.println("Fecha de contrato: " + fechaContrato);
        System.out.println("Pago diario base: " + pagoDiarioBase);
        System.out.println("Subtotal: " + calcularSubtotal());
        System.out.println("Impuesto " + calcularImpuesto());
        System.out.println("Total a pagar " + calcularTotalAPagar());
    }

    }
