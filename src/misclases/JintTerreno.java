/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

import javax.swing.JOptionPane;

/**
 *
 * @author elpep
 */
public class JintTerreno extends javax.swing.JInternalFrame {

    /**
     * Creates new form jintTerreno
     */
    public JintTerreno() {
        initComponents();
        this.desabilitar();
        this.resize(900,600);
    }
        public void desabilitar(){
            this.txtAncho.setEnabled(false);
            this.txtArea.setEnabled(false);
            this.txtLargo.setEnabled(false);
            this.txtPerim.setEnabled(false);
            
            //botones
            this.btnMostrar.setEnabled(false);
            this.btnGuardar.setEnabled(false);
            
        }
        public void habilitar(){
            this.txtAncho.setEnabled(!false);
            this.txtLargo.setEnabled(!false);
            this.btnGuardar.setEnabled(true);
        }
        public void limpiar(){
            this.txtAncho.setText("");
            this.txtLargo.setText("");
            this.txtArea.setText("");
            this.txtPerim.setText("");
            
            this.txtLargo.requestFocus();
        }
  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtLargo = new javax.swing.JTextField();
        txtAncho = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtPerim = new javax.swing.JTextField();
        txtArea = new javax.swing.JTextField();
        btnMostrar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnLmpiar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();

        setBackground(new java.awt.Color(254, 254, 242));
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Terreno");
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Segoe UI Historic", 1, 18)); // NOI18N
        jLabel1.setText("Largo =");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(40, 60, 80, 30);

        jLabel2.setFont(new java.awt.Font("Segoe UI Historic", 1, 18)); // NOI18N
        jLabel2.setText("Ancho =");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(40, 140, 90, 30);

        txtLargo.setFont(new java.awt.Font("Segoe UI Historic", 1, 18)); // NOI18N
        getContentPane().add(txtLargo);
        txtLargo.setBounds(140, 60, 60, 40);

        txtAncho.setFont(new java.awt.Font("Segoe UI Historic", 1, 18)); // NOI18N
        getContentPane().add(txtAncho);
        txtAncho.setBounds(140, 140, 60, 40);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Calculos del terreno", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI Historic", 1, 14), new java.awt.Color(239, 18, 24))); // NOI18N
        jPanel1.setLayout(null);

        jLabel3.setFont(new java.awt.Font("Segoe UI Historic", 1, 18)); // NOI18N
        jLabel3.setText("Perimetro :");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(40, 30, 100, 30);

        jLabel4.setFont(new java.awt.Font("Segoe UI Historic", 1, 18)); // NOI18N
        jLabel4.setText("Area :");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(80, 80, 60, 30);

        txtPerim.setFont(new java.awt.Font("Segoe UI Historic", 1, 18)); // NOI18N
        jPanel1.add(txtPerim);
        txtPerim.setBounds(170, 30, 60, 40);

        txtArea.setFont(new java.awt.Font("Segoe UI Historic", 1, 18)); // NOI18N
        jPanel1.add(txtArea);
        txtArea.setBounds(170, 80, 60, 40);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(270, 60, 270, 140);

        btnMostrar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnMostrar.setText("Mostrar");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnMostrar);
        btnMostrar.setBounds(440, 30, 80, 23);

        btnNuevo.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(270, 30, 65, 23);

        btnGuardar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(350, 30, 80, 23);

        btnLmpiar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnLmpiar.setText("Limpiar");
        btnLmpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLmpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLmpiar);
        btnLmpiar.setBounds(320, 230, 80, 23);

        btnCerrar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(140, 230, 80, 23);

        btnCancelar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(230, 230, 80, 23);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        // TODO add your handling code here:
        
        this.txtAncho.setText(String.valueOf(con.getAncho()));
        this.txtLargo.setText(String.valueOf(con.getLargo()));
        
        this.txtArea.setText(String.valueOf(con.calcularArea()));
        this.txtPerim.setText(String.valueOf(con.calcularPerimetro()));
        
        
    
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
         con = new Terreno();//se contrulle el objeto 
        this.habilitar();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
           boolean terre = false;
            if (this.txtAncho.getText().equals(""))terre = true;
            if (this.txtLargo.getText().equals(""))terre = true;
           
            if (terre == true){
                JOptionPane.showMessageDialog(this, "falto capturar informacion");
            }
            
            else{
                con.setAncho(Integer.parseInt(this.txtAncho.getText()));
                con.setLargo(Integer.parseInt(this.txtLargo.getText()));
                
                JOptionPane.showMessageDialog(this, "Se guardo con exito la informacion");
                this.btnMostrar.setEnabled(true);
            }   
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnLmpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLmpiarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
    }//GEN-LAST:event_btnLmpiarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
        int opcion = 0;
        opcion = JOptionPane.showConfirmDialog(this, "Desea salir?", "Terreno", JOptionPane.YES_NO_OPTION);
        
        if (opcion == JOptionPane.YES_OPTION){
            this.dispose();
            
        }
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        this.desabilitar();
    }//GEN-LAST:event_btnCancelarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLmpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtAncho;
    private javax.swing.JTextField txtArea;
    private javax.swing.JTextField txtLargo;
    private javax.swing.JTextField txtPerim;
    // End of variables declaration//GEN-END:variables
    private Terreno con;
}
