
package misclases;

import javax.swing.JOptionPane;

/**
 *
 * @author elpep
 */
public class JintFactura extends javax.swing.JInternalFrame {

  
    public JintFactura() {
        initComponents();
        this.desabilitar();
        this.resize(907,504);
    }
    

    public void desabilitar(){
            this.txtFactura.setEnabled(false);
            this.txtDescripcion.setEnabled(false);
            this.txtDomicilio.setEnabled(false);
            this.txtFecha.setEnabled(false);
            this.txtImp.setEnabled(false);
            this.txtNombre.setEnabled(false);
            this.txtRfc.setEnabled(false);
            this.txtTotal1.setEnabled(false);
            this.txtTotalSin.setEnabled(false);
            
            //botones
            this.btnGuardar.setEnabled(false);
            this.btnMostrar.setEnabled(false);
            
    }
    public void habilitar(){
            this.txtFactura.setEnabled(!false);
            this.txtDescripcion.setEnabled(!false);
            this.txtDomicilio.setEnabled(!false);
            this.txtFecha.setEnabled(!false);
            this.txtNombre.setEnabled(!false);
            this.txtRfc.setEnabled(!false);
            this.txtTotalSin.setEnabled(!false);
            this.btnGuardar.setEnabled(true);
    }
    public void limpiar(){
    
            this.txtFactura.setText("");
            this.txtDescripcion.setText("");
            this.txtDomicilio.setText("");
            this.txtFecha.setText("");
            this.txtImp.setText("");
            this.txtNombre.setText("");
            this.txtRfc.setText("");
            this.txtTotal1.setText("");
            this.txtTotalSin.setText("");
            
            this.txtFactura.requestFocus();
    }
    
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtDescripcion = new javax.swing.JTextField();
        txtFecha = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtRfc = new javax.swing.JTextField();
        txtDomicilio = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtImp = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtTotal1 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtFactura = new javax.swing.JTextField();
        btnCerrar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        txtTotalSin = new javax.swing.JTextField();

        setBackground(new java.awt.Color(204, 255, 204));
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Factura");
        addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                formAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel1.setText("Fecha de venta :");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(570, 20, 150, 30);

        jLabel2.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel2.setText("Numero de factura :");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(10, 30, 170, 30);

        jLabel4.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel4.setText("Nombre del cliente :");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(10, 100, 170, 30);

        jLabel5.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel5.setText("Domicilio fiscal :");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(40, 240, 140, 30);

        jLabel6.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel6.setText("Descripcion :");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(70, 310, 110, 30);

        txtDescripcion.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        getContentPane().add(txtDescripcion);
        txtDescripcion.setBounds(190, 310, 140, 30);

        txtFecha.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        getContentPane().add(txtFecha);
        txtFecha.setBounds(720, 20, 140, 30);

        txtNombre.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        getContentPane().add(txtNombre);
        txtNombre.setBounds(190, 100, 140, 30);

        txtRfc.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        getContentPane().add(txtRfc);
        txtRfc.setBounds(190, 170, 140, 30);

        txtDomicilio.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        getContentPane().add(txtDomicilio);
        txtDomicilio.setBounds(190, 240, 140, 30);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Calculos de la Factura", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trebuchet MS", 1, 18))); // NOI18N
        jPanel2.setLayout(null);

        jLabel3.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel3.setText("Total a pagar :");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(80, 180, 130, 30);

        txtImp.setBackground(new java.awt.Color(204, 204, 204));
        txtImp.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        txtImp.setForeground(new java.awt.Color(255, 255, 255));
        txtImp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtImpActionPerformed(evt);
            }
        });
        jPanel2.add(txtImp);
        txtImp.setBounds(220, 110, 140, 30);

        jLabel9.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel9.setText("Impuesto :");
        jPanel2.add(jLabel9);
        jLabel9.setBounds(120, 110, 90, 30);

        txtTotal1.setBackground(new java.awt.Color(204, 204, 204));
        txtTotal1.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        txtTotal1.setForeground(new java.awt.Color(255, 255, 255));
        txtTotal1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotal1ActionPerformed(evt);
            }
        });
        jPanel2.add(txtTotal1);
        txtTotal1.setBounds(220, 180, 140, 30);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(350, 140, 430, 250);

        jLabel7.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel7.setText("RFC :");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(130, 170, 50, 30);

        txtFactura.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        txtFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtFacturaActionPerformed(evt);
            }
        });
        getContentPane().add(txtFactura);
        txtFactura.setBounds(190, 30, 140, 30);

        btnCerrar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnCerrar.setForeground(new java.awt.Color(0, 0, 204));
        btnCerrar.setText("Cerrar");
        btnCerrar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 51)));
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(540, 440, 80, 20);

        btnNuevo.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnNuevo.setForeground(new java.awt.Color(0, 0, 204));
        btnNuevo.setText("Nuevo");
        btnNuevo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 51)));
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(800, 160, 80, 50);

        btnGuardar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnGuardar.setForeground(new java.awt.Color(0, 0, 204));
        btnGuardar.setText("Guardar");
        btnGuardar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 51)));
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(800, 240, 80, 50);

        btnMostrar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnMostrar.setForeground(new java.awt.Color(0, 0, 204));
        btnMostrar.setText("Mostrar");
        btnMostrar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 51)));
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnMostrar);
        btnMostrar.setBounds(800, 320, 80, 50);

        btnLimpiar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnLimpiar.setForeground(new java.awt.Color(0, 0, 204));
        btnLimpiar.setText("Limpiar");
        btnLimpiar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 51)));
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(280, 440, 80, 20);

        btnCancelar.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(0, 0, 204));
        btnCancelar.setText("Cancelar");
        btnCancelar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 51)));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(410, 440, 80, 20);

        jLabel8.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel8.setText("Total venta :");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(70, 370, 110, 30);

        txtTotalSin.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        txtTotalSin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalSinActionPerformed(evt);
            }
        });
        getContentPane().add(txtTotalSin);
        txtTotalSin.setBounds(190, 370, 140, 30);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtImpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtImpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtImpActionPerformed

    private void txtTotalSinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalSinActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalSinActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
        
        int opcion = 0;
        opcion = JOptionPane.showConfirmDialog(this, "Desea salir?", "Factura", JOptionPane.YES_NO_OPTION);
        
        if (opcion == JOptionPane.YES_OPTION){
            this.dispose();
            
        }
        
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        con = new Factura();
        this.habilitar();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        
        boolean factu = false;
            if (this.txtDescripcion.getText().equals("")) factu = true;
            if (this.txtDomicilio.getText().equals("")) factu = true;
            if (this.txtFactura.getText().equals("")) factu = true;
            if (this.txtFecha.getText().equals("")) factu = true;
            if (this.txtNombre.getText().equals("")) factu = true;
            if (this.txtRfc.getText().equals("")) factu = true;
            if (this.txtTotalSin.getText().equals("")) factu = true;
            
            if (factu == true){
            //falto informacion
            JOptionPane.showMessageDialog(this, "falto capturar informacion");
            }
            else{
                con.setNumFactura(Integer.parseInt(this.txtFactura.getText()));
                con.setDescripcion(this.txtDescripcion.getText());
                con.setDomicilioFiscal(this.txtDomicilio.getText());
                con.setFechaVenta(this.txtFecha.getText());
                con.setNombreCliente(this.txtNombre.getText());
                con.setRfc(this.txtRfc.getText());
                con.setTotalVenta(Float.parseFloat(this.txtTotalSin.getText()));
              
                
                
                JOptionPane.showMessageDialog(this, "Se guardo con exito la informacion");
                this.btnMostrar.setEnabled(true);
            }
        
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        // TODO add your handling code here:
        
        this.txtDescripcion.setText(con.getDescripcion());
        this.txtDomicilio.setText(con.getDomicilioFiscal());
        this.txtFecha.setText(con.getFechaVenta());
        this.txtNombre.setText(con.getNombreCliente());
        this.txtRfc.setText(con.getRfc());
        this.txtTotalSin.setText(String.valueOf(con.getTotalVenta()));
        this.txtFactura.setText(String.valueOf(con.getNumFactura()));
        
        this.txtImp.setText(String.valueOf(con.calcularImpuesto()));
        this.txtTotal1.setText(String.valueOf(con.calcularTotalPagar()));
        
       
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.desabilitar();
        this.limpiar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtFacturaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFacturaActionPerformed

    private void txtTotal1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotal1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotal1ActionPerformed

    private void formAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_formAncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_formAncestorAdded


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtDescripcion;
    private javax.swing.JTextField txtDomicilio;
    private javax.swing.JTextField txtFactura;
    private javax.swing.JTextField txtFecha;
    private javax.swing.JTextField txtImp;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtRfc;
    private javax.swing.JTextField txtTotal1;
    private javax.swing.JTextField txtTotalSin;
    // End of variables declaration//GEN-END:variables

    private Factura con;
}
