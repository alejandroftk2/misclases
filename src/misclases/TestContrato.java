package misclases;
import java.util.Scanner;

public class TestContrato {
    public static void main(String[] args) {
        Contrato contrato = new Contrato();
        Scanner sc = new Scanner(System.in);
        int opcion = 0, numContrato = 0, tipoContrato = 0, nivelEstudios = 0, diasTrabajados = 0;
        String fechaContrato = "No especificado", nombreEmpleado = "No especificado", domicilio = "No especificado";
        float pagoDiarioBase = 0.0f;
        
        do{
         System.out.println("1.-Iniciar el objeto");
         System.out.println("2.-Cambiar numero de contrato");
         System.out.println("3.-Cambiar tipo de contrato");
         System.out.println("4.-Cambiar nivel de estudios");
         System.out.println("5.-Cambiar dias trabajados");
         System.out.println("6.-Cambiar fecha de contrato");
         System.out.println("7.-Cambiar nombre de empleado");
         System.out.println("8.-Cambiar domicilio");
         System.out.println("9.-Cambiar pago diario base");
         System.out.println("10.-Mostrar informacion");
         System.out.println("11.-Salir");
         System.out.print("Escribe la opcion: ");
         opcion = sc.nextInt();
         
         switch(opcion){
             case 1:
                 System.out.println("Escribe el numero de contrato");
                 numContrato = sc.nextInt();
                 System.out.println("Escribe el numero de tipo de contrato:\n1.-Temporal\n2.-Base");
                 tipoContrato = sc.nextInt();
                 System.out.println("Escribe el numero de nivel de estudios:\n1.-Licenciatura\n2.-Maestria\n3.-Doctorado");
                 nivelEstudios = sc.nextInt();
                 System.out.println("Escribe los dias trabajados");
                 diasTrabajados = sc.nextInt();
                 sc.nextLine();
                 System.out.println("Escribe el nombre del empleado");
                 nombreEmpleado = sc.nextLine();
                 System.out.println("Escribe fecha de contrato");
                 fechaContrato = sc.nextLine();
                 System.out.println("Escribe el domicilio");
                 domicilio = sc.nextLine();
                 System.out.println("Escribe pago diario base");
                 pagoDiarioBase = sc.nextFloat();
                 contrato.setNumeroContrato(numContrato);
                 contrato.setTipoContrato(tipoContrato);
                 contrato.setNivelEstudios(nivelEstudios);
                 contrato.setDiasTrabajados(diasTrabajados);
                 contrato.setFechaContrato(fechaContrato);
                 contrato.setNombreEmpleado(nombreEmpleado);
                 contrato.setDomicilio(domicilio);
                 contrato.setPagoDiarioBase(pagoDiarioBase);
                 break;
             case 2:
                 System.out.println("Escribe el nuevo numero de contrato");
                 numContrato = sc.nextInt();
                 contrato.setNumeroContrato(numContrato);
                 break;
             case 3:
                 System.out.println("Escribe el nuevo tipo de contrato\n1.-Temporal\n2.-Base");
                 tipoContrato = sc.nextInt();
                 contrato.setTipoContrato(tipoContrato);
                 break;
             case 4:
                 System.out.println("Escribe el nuevo nivel de estudios\n1.-Licenciatura\n2.-Maestria\n3.-Doctorado");
                 nivelEstudios = sc.nextInt();
                 contrato.setNivelEstudios(nivelEstudios);
                 break;
             case 5:
                 System.out.println("Escriba los dias trabajados");
                 diasTrabajados = sc.nextInt();
                 contrato.setDiasTrabajados(diasTrabajados);
                 break;
             case 6:
                 System.out.println("Escribe la fecha del contrato");
                 fechaContrato = sc.nextLine();
                 contrato.setFechaContrato(fechaContrato);
                 break;
             case 7:
                 System.out.println("Escribe el nombre del empleado");
                 nombreEmpleado = sc.nextLine();
                 contrato.setNombreEmpleado(nombreEmpleado);
                 break;
             case 8:
                 System.out.println("Escribe el domicilio");
                 domicilio = sc.nextLine();
                 contrato.setDomicilio(domicilio);
                 break;
             case 9:
                 System.out.println("Escribe el pago diario base");
                 pagoDiarioBase = sc.nextFloat();
                 contrato.setPagoDiarioBase(pagoDiarioBase);
                 break;
             case 10:
                 contrato.imprimirContrato();
                 break;
             case 11:
                 System.out.println("Adios");
                 break;
             default:
                 System.out.println("Opcion invalida use una del rango valido");
         }
           
        }while(opcion!=11);
        
        
    }
}
