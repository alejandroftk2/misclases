/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author elpep
 */
public class Nomina {
    private int numNOmina;
    private String nombre;
    private int nivel;
    private int pagoHora;
    private int horasTabajadas;

    public Nomina() {
    }

    public Nomina(int numNOmina, String nombre, int nivel, int pagoHora, int horasTabajadas) {
        this.numNOmina = numNOmina;
        this.nombre = nombre;
        this.nivel = nivel;
        this.pagoHora = pagoHora;
        this.horasTabajadas = horasTabajadas;
    }

    public int getNumNOmina() {
        return numNOmina;
    }

    public void setNumNOmina(int numNOmina) {
        this.numNOmina = numNOmina;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(int pagoHora) {
        this.pagoHora = pagoHora;
    }

    public int getHorasTabajadas() {
        return horasTabajadas;
    }

    public void setHorasTabajadas(int horasTabajadas) {
        this.horasTabajadas = horasTabajadas;
    }
    
    
    
    
}
