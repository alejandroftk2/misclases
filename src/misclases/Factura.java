/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author elpep
 */
public class Factura {
    private int numFactura;
    private String rfc;
    private String nombreCliente;
    private String domicilioFiscal;
    private String descripcion;
    private String fechaVenta;
    private float totalVenta;

    public Factura() {
        //constructor por omision
            this.numFactura =0;
            this.rfc ="";
            this.nombreCliente ="";
            this.domicilioFiscal ="";
            this.descripcion ="";
            this.fechaVenta ="";
            this.totalVenta =0.0f;
    }

    public Factura(int numFactura, String rfc, String nombreCliente, String domicilioFiscal, String descripcion, String fechaVenta, float totalVenta) {
        //construccion por argumento
        this.numFactura = numFactura;
        this.rfc = rfc;
        this.nombreCliente = nombreCliente;
        this.domicilioFiscal = domicilioFiscal;
        this.descripcion = descripcion;
        this.fechaVenta = fechaVenta;
        this.totalVenta = totalVenta;
    }
    public Factura(Factura otro){
        //constructor por copia
        this.numFactura = otro.numFactura;
        this.rfc = otro.rfc;
        this.nombreCliente = otro.nombreCliente;
         this.domicilioFiscal = otro.domicilioFiscal;
        this.descripcion = otro.descripcion;
        this.fechaVenta = otro.fechaVenta;
        this.totalVenta = otro.totalVenta;
    }

    public int getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(int numFactura) {
        this.numFactura = numFactura;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDomicilioFiscal() {
        return domicilioFiscal;
    }

    public void setDomicilioFiscal(String domicilioFiscal) {
        this.domicilioFiscal = domicilioFiscal;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(String fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    public float getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(float totalVenta) {
        this.totalVenta = totalVenta;
   }
    
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = this.totalVenta * .16f;
        return impuesto;
    }
    public float calcularTotalPagar(){
        float total = 0.0f;
        total = this.totalVenta + this.calcularImpuesto();
        return total;
    }
    public void imprimirFactura(){
        System.out.println(" numero de factuta: " + this.numFactura);
        System.out.println(" el rfc es: " + this.rfc);
        System.out.println(" Nombre del cliente: " + this.nombreCliente);
        System.out.println(" Domicilio fiscal " + this.domicilioFiscal);
        System.out.println(" Descripcion: " + this.descripcion);
        System.out.println(" fecha de la venta: " + this.fechaVenta);
        System.out.println(" total de venta sin impuesto = " + this.totalVenta);
        System.out.println(" Impuesto " + this.calcularImpuesto());
        System.out.println(" Total a pagar " + this.calcularTotalPagar());
    }
    
}
